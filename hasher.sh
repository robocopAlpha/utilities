#!/usr/bin/env bash
HELP()
{
printf 'hasher - allows you to compute md5, xxh and sha checksum of entered text
Usage:
   hasher md5

OPTIONS:     
    --help         Show help
    md5            Calculate md5
    sha            Calculate shasum (given options)
    xxh         Calculate xxhsum 
    
Follow the development: https://gitlab.com/robocopAlpha/utilities
Copyright 2020, Deepankar Chakroborty. All rights reserved.
';
  
}

if [ "$#" -eq 0 ]; then
    HELP
    exit 0;
fi
if [ "$1" == "--help" ]; then
	HELP
	exit 0;
elif [ "$1" == "md5" ]; then
	read -r -p "Enter text for hashing: " text;
	hash=$(md5 -s $text | cut -d'=' -f 2 | tr -d ' ');
	echo "MD5($text) : $hash"
	echo "$hash" | pbcopy
	exit 0;
elif [ "$1" == "sha" ]; then
	read -r -p "Enter text for hashing: " text;
	read -r -p 'Enter SHA variant (1,224, 256, 384, 512, 512224, 512256 (default): ' var;
	if [ -z "${var}" ]; then
		hash=$(printf $text | shasum -a 512256 | cut -d " " -f 1);
		echo "SHA-512256($text) : $hash"
		echo "$hash" | pbcopy
		exit 0;
	else
		hash=$(printf $text | shasum -a $var | cut -d " " -f 1);
		echo "SHA-$var($text) : $hash"
		echo "$hash" | pbcopy
 		exit 0;
	fi
elif [ "$1" == "xxh" ]; then
	read -r -p "Enter text for hashing: " text;
	printf '
Select size
0 - [32 bits]
1 - [64 bits]
2 - [128 bits] 
(default: 1 - 64 bits) Enter: ';
	read var;
	if [ -z "${var}" ]; then
		hash=$(printf $text | xxhsum -H1 | cut -d " " -f 1); 
		echo "xxh${var}($text) : $hash"
		echo "$hash" | pbcopy
		exit 0;
	else
		hash=$(printf $text | xxhsum -H$var | cut -d " " -f 1);
		echo "xxh-${var}($text) : $hash"
		echo "$hash" | pbcopy
		exit 0;
	fi
else
	echo "Incorrect input. Aborting"
	HELP
	exit 1;
fi
