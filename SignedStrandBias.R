strandBias=function(arg1,arg2){
  if(length(arg1)!=length(arg2)){
    error=paste("Parameters with unequal lengths passed as argument!\narg1 =",length(arg1),"; arg2 =",length(arg2))
    message(error)
    return(NA)
  }
  SB=c()
  for (i in seq(1,length(arg1))){
    if(is.na(arg1[i]) | is.na(arg2[i])){
      calc=NA
    }
    else if(arg1[i]<arg2[i]){
      calc=(arg2[i]/arg1[i])*-1
    }
    else{
      calc=(arg1[i]/arg2[i])
    }
    SB=c(SB,calc)
  }
  return(SB)
}
